package de.awacademy.fussballmanager;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Optional;

@Controller
public class ManagerController {

    ArrayList<Club> clubs = new ArrayList<>();

    @GetMapping("/")
    public String index(Model model){
        Collections.sort(clubs);
        model.addAttribute("clubs", clubs);
        return "index";
    }

    @GetMapping("/club-detail")
    public String clubDetail(@RequestParam int clubId, Model model){
        Collections.sort(clubs.get(clubId).getRoster());
        model.addAttribute("club",clubs.get(clubId));
        model.addAttribute("clubId", clubId);
        return "club-detail";
    }

    @GetMapping("/player-detail")
    public String playerDetail(@RequestParam int clubId, @RequestParam int playerId,
                               Model model){
        model.addAttribute("playerId", playerId);
        model.addAttribute("player", clubs.get(clubId).getRoster().get(playerId));
        return "player-detail";
    }

    @GetMapping("/new-club")
    public String newClub(Model model){
        model.addAttribute("club", new Club(""));
        return "new-club";
    }

    @PostMapping("/submit-club")
    public String submitClub(@ModelAttribute Club generatedClub){
        clubs.add(generatedClub);
        return "redirect:/";
    }

    @GetMapping("/new-player")
    public String newPlayer(@RequestParam int clubId, Model model){
        model.addAttribute("player", new Player(""));
        model.addAttribute("clubId", clubId);
        return "new-player";
    }

    @PostMapping("/submit-player")
    public String submitPlayer(@ModelAttribute Player generatedPlayer, @RequestParam int clubId){
        generatedPlayer.setPlaysAt(clubs.get(clubId).getName());
        clubs.get(clubId).getRoster().add(generatedPlayer);
        return "redirect:/club-detail?clubId=" + clubId;
    }

    @PostMapping("/delete-player")
    public String deletePlayer(@RequestParam int clubId, @RequestParam int playerId){
        clubs.get(clubId).getRoster().remove(playerId);
        return "redirect:/club-detail?clubId=" + clubId;
    }

    @GetMapping ("/edit-player")
    public String editPlayer(Model model, @RequestParam int clubId, @RequestParam int playerId){
        model.addAttribute("player", clubs.get(clubId).getRoster().get(playerId));
        ArrayList<Club> selectList = new ArrayList<>(clubs);
        Collections.swap(selectList,0,clubId);
        if(selectList.size() > 2){
            Collections.sort(selectList.subList(1,clubs.size() - 1));
        }
        model.addAttribute("selectList", selectList);
        model.addAttribute("clubs", clubs);
        model.addAttribute("clubId", clubId);
        model.addAttribute("playerId", playerId);
        return "edit-player";
    }

    @PostMapping("/change-player-data")
    public String changePlayerData(@ModelAttribute Player generierterPlayer,
                                   @RequestParam int clubId, @RequestParam int playerId){
        clubs.get(clubId).getRoster().remove(playerId);
        Optional<Club> playerClub = clubs.stream()
                .filter(p -> p.getName().equals(generierterPlayer.getPlaysAt())).findFirst();
        playerClub.get().getRoster().add(generierterPlayer);
        return "redirect:/club-detail?clubId="+ clubs.indexOf(playerClub.get());
    }
}

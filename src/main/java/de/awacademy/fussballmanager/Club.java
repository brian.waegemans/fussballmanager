package de.awacademy.fussballmanager;

import java.util.ArrayList;

public class Club implements Comparable<Club> {
    private String name;
    private int points;
    private ArrayList<Player> roster;

    public Club(String name) {
        this.name = name;
        this.points = 0;
        this.roster = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public ArrayList<Player> getRoster() {
        return roster;
    }

    @Override
    public int compareTo(Club otherClub) {
        return Integer.compare(otherClub.getPoints(), getPoints());
    }
}

package de.awacademy.fussballmanager;

public class Player implements Comparable<Player>{
    private String name;
    private int number;
    private String playsAt;

    public Player(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPlaysAt() {
        return playsAt;
    }

    public void setPlaysAt(String playsAt) {
        this.playsAt = playsAt;
    }

    @Override
    public int compareTo(Player otherPlayer) {
        return Integer.compare(this.getNumber(), otherPlayer.getNumber());
    }
}
